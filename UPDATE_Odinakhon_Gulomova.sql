-- Update the rental duration and rental rate of the film "Inception"
UPDATE film
SET rental_duration = 5, rental_rate = 7.99
WHERE title = 'Inception';

-- Update the personal data of the customer
UPDATE customer
SET first_name = 'John', last_name = 'Doe', email = 'johndoe@example.com', address_id = (SELECT address_id FROM address ORDER BY RANDOM() LIMIT 1)
WHERE customer_id IN (
    SELECT rental.customer_id
    FROM rental
    JOIN payment ON rental.customer_id = payment.customer_id
    GROUP BY rental.customer_id
    HAVING COUNT(DISTINCT rental.rental_id) >= 5 AND COUNT(DISTINCT payment.payment_id) >= 5
);

-- Update the create_date of the customer
UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id IN (
    SELECT rental.customer_id
    FROM rental
    JOIN payment ON rental.customer_id = payment.customer_id
    GROUP BY rental.customer_id
    HAVING COUNT(DISTINCT rental.rental_id) >= 5 AND COUNT(DISTINCT payment.payment_id) >= 5
);
